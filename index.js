//Mock database
let posts = [];
//count variable that will serve as the post ID
let count = 1;

// Add post data
document.querySelector('#form-add-post').
	addEventListener('submit', (e) =>{
		//prevent the page from loading
		e.preventDefault();

		posts.push({
			id: count, 
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value
		})
	// Increment the id value in count variable
	count++;

	showPosts(posts);
	alert('Sucessfully added.')

});

//Show Posts
const showPosts = (posts) => {
	// Sets the HTML structure to display posts.
	let postEntries = '';

	posts.forEach((post) =>{
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost(${post.id})">Edit</button>
				<button onclick="deletePost(${post.id})">Delete</button>
			</div>
		`;
	})

	document.querySelector('#div-post-entries').innerHTML = postEntries
}


//Edit Post

const editPost = (id) =>{
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
}

//Update Post

document.querySelector('#form-edit-post').addEventListener('submit', (e) =>{
	e.preventDefault();

	//for loop was used to search for the value to query until satisfied
	// toString is to convert the number
	for(let i = 0; i < posts.length; i++){
		if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value){
			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;
		

		showPosts(posts);
		alert('Sucessfully updated');

		break;


		}
	}
})

// Delete post data

const deletePost = (id) =>{

	posts = posts.filter((post) =>{
		if (post.id.toString() !== id){
			posts.splice(id,1)
		}
	});

	document.querySelector(`#post-${id}`).remove();
		
	console.log(posts)
	alert('Sucessfully deleted.')
	}


